#!/bin/sh

# INFO FOR PROGRAM TO BE BUILT
THIS_NAME="$(basename $0)"
THIS_PATH="$(cd $(dirname $0);pwd)"
PROG_NAME="$(basename $THIS_NAME .SlackBuild)"
PROG_VERS="0.1.10-alpha" # CHANGE TO WHAT WE NEED
PROG_FULL="$PROG_NAME-$PROG_VERS" # SHOULD BE TOP FOLDER'S NAME
PROG_BALL="$PROG_FULL.tar.gz" # CAN BE OTHER THAN $PROG_FULL
PROG_FOLD="$PROG_FULL" # CAN BE OTHER THAN $PROG_FULL
PROG_INFO="colobot-gold-music (Music data for colobot-gold)"
PROG_SITE="http://colobot.info/"
# PLACE URL ACCORDINGLY!
PROG_PATH="http://colobot.info/files/music/"
PROG_LOAD="${PROG_PATH}${PROG_BALL//-gold-music-/-music_ogg_}"
# ASSIGN DOCS ACCORDINGLY!
PROG_DOCS="LICENSE.txt"
# DEPS LIST? (build-time?)
PROG_DEPS=""

# TEMPLATE VARIABLES - NO CHANGES NEEDED

# CHECK MACHINE ARCH!
if [ "$ARCH" == "" ]; then
	ARCH="$(uname -m)"
	case "$ARCH" in
		i?86) ARCH="i486" ;;
		arm*) ARCH="arm" ;;
	esac
fi
# OUTPUT PACKAGE PARAMS
BUILD_TAG=${BUILD_TAG:="1_my1"}
PACK_TYPE=${PACK_TYPE:="tgz"}
PACK_ARCH=${PACK_ARCH:="$ARCH"}
PACK_PATH=${PACK_PATH:="/tmp/my1"}
PACK_BALL=${PACK_BALL:="$PROG_NAME-$PROG_VERS-$PACK_ARCH-$BUILD_TAG.$PACK_TYPE"}
# BUILD-RELATED PATH
CURR_PATH=$(pwd)
TEMP_PATH=${TEMP_PATH:="/tmp/my1/slackbuild"}
DEST_PATH=$TEMP_PATH/package-$PROG_NAME
# COMMAND-LINE OPTIONS
CHK_DOWNLOAD=${CHK_DOWNLOAD:="NO"}
CHK_LOADONLY=${CHK_LOADONLY:="NO"}
CHK_MOVEPACK=${CHK_MOVEPACK:="NO"}
CHK_INSTPACK=${CHK_INSTPACK:="NO"}
CHK_BUILDDEP=${CHK_BUILDDEP:="NO"}

# CUSTOM BUILD FUNCTIONS - CHANGE ACCORDINGLY

prepare_source()
{
	# PREPARE SOURCE MANUALLY
	return 0
}

post_source()
{
	# MODIFY/REPACK TARBALL
	rm -rf $PROG_FOLD
	mkdir $PROG_FOLD ; cd $PROG_FOLD
	tar xvf $CURR_PATH/$PROG_BALL >/dev/null
	cd ..
	if [ -f "${PROG_FOLD}/LICENSE.txt" ] ; then
		tar cf $CURR_PATH/$PROG_BALL.new $PROG_FOLD
		local chk_perm=$(get_userngrp $CURR_PATH)
		[ "$chk_perm" != "" ] && chown $chk_perm $CURR_PATH/$PROG_BALL.new
		if [ $? -eq 0 ] ; then
			mv $CURR_PATH/$PROG_BALL.new $CURR_PATH/$PROG_BALL
		else
			rm $CURR_PATH/$PROG_BALL.new
		fi
	fi
	rm -rf $PROG_FOLD
}

do_build()
{
	# ACTUAL BUILD PROCESS
	local here=`pwd`
	# simply copy actually
	mkdir -p "$DEST_PATH/usr/share/games/colobot/music"
	for ogg in `find $here -name "*.ogg"` ; do
		cp $ogg "$DEST_PATH/usr/share/games/colobot/music/"
	done
}

do_pre_build()
{
	# PLACE FIX/PRE-BUILD SCRIPT HERE!
	return
}

do_post_build()
{
	# PLACE FIX/POST-BUILD SCRIPT HERE!
	return
}

do_install_stuff()
{
	# COPY INSTALLATION-RELATED FILE(S)
	return
}

do_post_package()
{
	# PLACE POST-PACKAGE SCRIPT HERE!
	return
}

# ONLY TEMPLATES BELOW THIS LINE

show_deps() {
	local buildscript warez pkg_temp pkg_root
	if [ ! -z "$PROG_DEPS" ] ; then
		echo "This software '$PROG_NAME' requires: "
		for warez in $PROG_DEPS ; do
			echo -n "$warez "
			buildscript="${THIS_PATH}/${warez}.SlackBuild"
			# look for installed only!
			pkg_temp="^${warez}-[^-]*-[^-]*-[^-]*$"
			pkg_root=$(ls /var/log/packages | grep -e "$pkg_temp")
			if [ ! -z "$pkg_root" ] ; then
				# check version
				pkg_temp=$(echo $pkg_root | sed 's/\(.*\)-\(.*\)/\1/')
				pkg_temp=$(echo $pkg_temp | sed 's/\(.*\)-\(.*\)/\1/')
				pkg_temp=$(echo $pkg_temp | sed 's/\(.*\)-\(.*\)/\2/')
				echo "(installed!{$pkg_temp})"
			elif [ -r "$buildscript" ] ; then
				echo "($buildscript)"
			else
				echo "(not found!)"
			fi
		done
	fi
}

build_deps() {
	local buildscript warez
	if [ "$PROG_DEPS" != "" ] ; then
		echo "Building software in dependency list..."
		for warez in $PROG_DEPS ; do
			echo -n "$warez "
			buildscript="${THIS_PATH}/${warez}.SlackBuild"
			if [ -r "$buildscript" ] ; then
				echo "($buildscript)"
				[ ! -x "$buildscript" ] &&
					echo "Cannot execute $buildscript!" && exit 1
				$buildscript $@
			else
				echo "(not found!)"
			fi
		done
	fi
}

get_move_path() {
	[ ! -d "$1" ] && echo "Invalid path for '--move'! ($1)" && exit 1
	MOVE_PATH=$(cd $1;pwd -P)
}

check_param() {
	while [ ! -z "$1" ]; do
		case $1 in
			--load) CHK_DOWNLOAD="YES" ;;
			--load-only) CHK_DOWNLOAD="YES" ; CHK_LOADONLY="YES" ;;
			--move-here) MOVE_PATH=$CURR_PATH ; CHK_MOVEPACK="YES" ;;
			--move) shift ; get_move_path $1 ; CHK_MOVEPACK="YES" ;;
			--install) CHK_INSTPACK="YES" ;;
			--show-deps) show_deps ; exit 0 ;;
			# auto-build deps, imply --install
			--build-deps) CHK_BUILDDEP="YES" ;;
			-*|*) echo "Unknown parameter ($1)!" && exit 1 ;;
		esac
		shift
	done
	# set these as early as possible?
	set -e # exit immediately when non-zero exit status
	trap 'echo "$0 FAILED at line $LINENO!"' ERR
}

do_download() {
	local fname label cpath cperm
	fname="$1"
	label="$2"
	cpath="$(pwd)"
	[ -z "$fname" ] && exit 1 # shouldn't be here?
	[ -z "$label" ] && label="$fname"
	echo -n "Downloading $label:     "
	wget --progress=dot -O $label $fname 2>&1 | \
		grep --line-buffered "% " | \
		sed -u -e "s,\.,,g" | awk '{printf("\b\b\b\b%4s", $2)}'
	[ ! -r "$label" ] &&
		echo "Cannot download source $fname to $cpath/$label!" && exit 1
	echo -ne "\b\b\b\b DONE!\n"
	cperm=$(get_userngrp $cpath)
	[ ! -z "$cperm" ] && chown $cperm $cpath/$label
}

check_source() {
	if [ ! -r "$PROG_BALL" ]; then
		[ "$CHK_DOWNLOAD" != "YES" ] &&
			echo "Source $CURR_PATH/$PROG_BALL not found!" && exit 1
		do_download $PROG_LOAD $PROG_BALL
	fi
	[ "$CHK_LOADONLY" == "YES" ] && exit 0
	return 0 # need this here or bash will fail execution
}

check_buildenv() {
	# CHECK ENVIRONMENT!
	LIBDIRSUFFIX=
	ARCHQUADLET=
	case "$PACK_ARCH" in
		"i486") SLACKCFLAGS="-O2 -march=i486 -mtune=i686" ;;
		"i686") SLACKCFLAGS="-O2 -march=i686 -mtune=i686" ;;
		"x86_64") SLACKCFLAGS="-O2 -fPIC" ; LIBDIRSUFFIX="64" ;;
		"arm") SLACKCFLAGS="-O2 -march=armv4t"; ARCHQUADLET="-gnueabi" ;;
		*) SLACKCFLAGS="-O2" ;;
	esac
}

prepare_build() {
	rm -rf $DEST_PATH
	mkdir -p $TEMP_PATH $DEST_PATH $PACK_PATH
	cd $TEMP_PATH
	rm -rf $PROG_FOLD
	tar xvf $CURR_PATH/$PROG_BALL >${PROG_NAME}-extract.log
	[ ! -d $PROG_FOLD ] && echo "Cannot find path '$PROG_FOLD'!" && exit 1
	cd $PROG_FOLD
	chown -R root:root .
	chmod -R u+r+w,go+r-w,a+X-s-t .
}

strip_binaries() {
	find $DEST_PATH | xargs file | grep -e "executable" | grep ELF \
		| cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
	find $DEST_PATH | xargs file | grep -e "shared object" | grep ELF \
		| cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true
}

check_manual_info() {
	# PREPARE MAN PAGES?
	if [ -d $DEST_PATH/usr/man ]; then
		find $DEST_PATH/usr/man -type f -exec gzip -9 {} \;
		for i in $(find $DEST_PATH/usr/man -type l) ; do
			ln -s $(readlink $i).gz $i.gz
			rm $i
		done
	fi
	# PREPARE INFO PAGES?
	if [ -d $DEST_PATH/usr/info ]; then
		gzip -9 $DEST_PATH/usr/info/*.info
		rm -f $DEST_PATH/usr/info/dir
	fi
}

check_documentation() {
	mkdir -p $DEST_PATH/usr/doc/$PROG_FULL
	cat $CURR_PATH/$THIS_NAME > $DEST_PATH/usr/doc/$PROG_FULL/$THIS_NAME
	[ -z "$PROG_DOCS" ] && return
	cp -a $PROG_DOCS $DEST_PATH/usr/doc/$PROG_FULL
}

do_install_first() {
	# COPY INSTALLATION-RELATED FILE(S)
	mkdir -p $DEST_PATH/install
	RULER_SPC="$(printf "%*s" ${#PROG_NAME} "")";
	RULER_HYP="$(printf "%*s" 70 "" | tr ' ' '-')";
	echo "${RULER_SPC}${RULER_HYP}" > $DEST_PATH/install/slack-desc
	echo "${PROG_NAME}: ${PROG_INFO}" >> $DEST_PATH/install/slack-desc
	echo "${PROG_NAME}:" >> $DEST_PATH/install/slack-desc
	echo "${PROG_NAME}: ${PROG_SITE}" >> $DEST_PATH/install/slack-desc
	echo "${PROG_NAME}:" >> $DEST_PATH/install/slack-desc
	echo "${PROG_NAME}: ${PROG_MORE}" >> $DEST_PATH/install/slack-desc
	echo "${PROG_NAME}:" >> $DEST_PATH/install/slack-desc
	echo "${PROG_NAME}:" >> $DEST_PATH/install/slack-desc
	echo "${PROG_NAME}:" >> $DEST_PATH/install/slack-desc
	echo "${PROG_NAME}:" >> $DEST_PATH/install/slack-desc
	echo "${PROG_NAME}:" >> $DEST_PATH/install/slack-desc
	echo "${PROG_NAME}:" >> $DEST_PATH/install/slack-desc
}

get_userngrp() {
	local chk_this chk_path chk_user chk_grpn
	chk_this=$1
	[ "$chk_this" == "." ] && chk_this=$(pwd)
	[ "$chk_this" == ".." ] && chk_this=$(cd .. ;pwd)
	[ ! -r "$chk_this" ] && echo -e "Cannot read '$chk_this'!" 1>&2 && return
	chk_path=$(cd $(dirname $chk_this);pwd)
	chk_this=$(basename $chk_this)
	chk_user=$(ls -l "${chk_path}/" | grep -e "${chk_this}$")
	set -- $chk_user
	chk_user=$3
	chk_grpn=$4
	echo -n "$chk_user:$chk_grpn"
}

make_slackpkg() {
	local SELECT_PATH chk_perm
	SELECT_PATH=$(cd $PACK_PATH;pwd -P)
	cd $DEST_PATH
	/sbin/makepkg -l y -c n $PACK_PATH/$PACK_BALL
	if [ "$CHK_MOVEPACK" == "YES" ]; then
		chk_perm=$(get_userngrp $MOVE_PATH)
		[ "$chk_perm" != "" ] && chown $chk_perm $PACK_PATH/$PACK_BALL
		mv $PACK_PATH/$PACK_BALL $MOVE_PATH/
		echo -e "\nPackage $PACK_PATH/$PACK_BALL moved to $MOVE_PATH!\n"
		SELECT_PATH=$MOVE_PATH
	fi
	if [ "$CHK_INSTPACK" == "YES" ]; then
		installpkg $SELECT_PATH/$PACK_BALL
		echo -e "\nPackage $SELECT_PATH/$PACK_BALL installed!\n"
	fi
	cd $CURR_PATH
}

# CHECK COMMAND-LINE PARAMETER - TEMPLATE
check_param $@
# IN CASE SOURCE FROM REPO - CUSTOM (OPTIONAL)
prepare_source
# CHECK IF SOURCE EXISTS - TEMPLATE
check_source
# IN CASE NEED TO MODIFY TARBALL - CUSTOM (OPTIONAL)
post_source
# ONLY ROOT CAN BUILD!
[ $UID -ne 0 ] && echo "[ERROR] Must run as root! Aborting!" && exit 1
# OPTION TO AUTOBUILD DEPENCY (--install IMPLIED)
[ "$CHK_BUILDDEP" == "YES" ] && build_deps $@ --install
# CHECK THE BUILD ENVIRONMENT - TEMPLATE
check_buildenv
# PREPARE FOR THE BUILD - TEMPLATE
prepare_build
# EXTRA STUFF - CUSTOM (OPTIONAL)
do_pre_build
# ACTUAL BUILD PROCESS - CUSTOM (STANDARD)
do_build
# EXTRA STUFF - CUSTOM (OPTIONAL)
do_post_build
# STRIP SIZE OF BINARIES - TEMPLATE
strip_binaries
# PREPARE MAN & INFO PAGES - TEMPLATE
check_manual_info
# COPY DOCUMENTATION FILE(S) - TEMPLATE
check_documentation
# PREP FOR INSTALLATION - TEMPLATE
do_install_first
# PREP FOR INSTALLATION - CUSTOM (STANDARD)
do_install_stuff
# MAKE SLACKWARE PACKAGE - TEMPLATE
make_slackpkg
# EXTRA POST-PACKAGING - CUSTOM (OPTIONAL)
do_post_package
